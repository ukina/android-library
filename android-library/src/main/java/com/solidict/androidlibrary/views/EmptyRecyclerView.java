package com.solidict.androidlibrary.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.solidict.androidlibrary.R;

/**
 * Created by umutkina on 25/12/15.
 */
public class EmptyRecyclerView extends RecyclerView {
    private View emptyView;
    Context context;
    final private AdapterDataObserver observer = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            checkIfEmpty();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            checkIfEmpty();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            checkIfEmpty();
        }
    };

    public EmptyRecyclerView(Context context) {
        super(context);
        this.context = context;
    }

    public EmptyRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public EmptyRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
    }

    //    void checkIfEmpty() {
//        if (emptyView != null && getAdapter() != null) {
//
//
//            final boolean emptyViewVisible = getAdapter().getItemCount() == 0;
//            if (emptyViewVisible) {
//                addView(emptyView);
//            }
//            else{
//                removeView(emptyView);
//            }
//
//            emptyView.setVisibility(emptyViewVisible ? VISIBLE : GONE);
////            setVisibility(emptyViewVisible ? GONE : VISIBLE);
//        }
//    }
    void checkIfEmpty() {
        if (emptyView != null && getAdapter() != null) {
            final boolean emptyViewVisible = getAdapter().getItemCount() == 0;
            if (emptyViewVisible) {
                ViewGroup rootView = (ViewGroup) getParent();

               ViewGroup viewGroup= (ViewGroup) emptyView.getParent();
                if (viewGroup != null) {
                    ViewGroup.LayoutParams layoutParams = getLayoutParams();
                    emptyView.setLayoutParams(layoutParams);
                    viewGroup.removeView(emptyView);
                }
                rootView.addView(emptyView);

            }
            else{

            }
            emptyView.setVisibility(emptyViewVisible ? VISIBLE : GONE);
            setVisibility(emptyViewVisible ? GONE : VISIBLE);
        }
    }

//    public void setEmptyView(View emptyView) {
//        this.emptyView = emptyView;
//        checkIfEmpty();
//    }

    @Override
    public void setAdapter(Adapter adapter) {
        final Adapter oldAdapter = getAdapter();
        if (oldAdapter != null) {
            oldAdapter.unregisterAdapterDataObserver(observer);
        }
        super.setAdapter(adapter);
        if (adapter != null) {
            adapter.registerAdapterDataObserver(observer);
        }

        checkIfEmpty();
    }

    public void setEmptytext(String text) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.empty_recycler_text, null);

        TextView tvMessage = (TextView) view.findViewById(R.id.tv_empty_text);
        tvMessage.setText(text);
        view.setVisibility(GONE);
        this.emptyView = view;
        checkIfEmpty();
    }
}