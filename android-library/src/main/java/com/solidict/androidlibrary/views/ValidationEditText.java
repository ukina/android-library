package com.solidict.androidlibrary.views;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.solidict.androidlibrary.views.typefacedviews.TypafacedEdittext;

import java.util.regex.Pattern;

import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;

/**
 * Created by umutkina on 10/21/16.
 */

public class ValidationEditText extends TypafacedEdittext {
    public ValidationEditText(Context context, AttributeSet attrs) {
        super(context, attrs);


    }

    public  void  setValidation(Pattern pattern,String message){


        final AwesomeValidation mAwesomeValidation = new AwesomeValidation(BASIC);
//        mAwesomeValidation.setColor(Color.YELLOW);

        mAwesomeValidation.addValidation(this, pattern, message);


        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mAwesomeValidation.validate();
            }
        });
    }
}
