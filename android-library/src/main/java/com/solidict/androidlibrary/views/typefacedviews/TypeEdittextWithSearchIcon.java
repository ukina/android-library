package com.solidict.androidlibrary.views.typefacedviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.solidict.androidlibrary.R;
import com.solidict.androidlibrary.interfaces.SearchClickInterface;
import com.solidict.androidlibrary.utils.UtilsLibrary;


/**
 * Created by umutkina on 08/12/14.
 */
public class TypeEdittextWithSearchIcon extends EditText {
    private Drawable dRight;
    private Rect rBounds;
    Context context;

    public TypeEdittextWithSearchIcon(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        //Typeface.createFromAsset doesn't work in the layout editor. Skipping...
        if (isInEditMode()) {
            return;
        }
        setSingleLine(true);

        setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    setSelection(getText().toString().length());
                }
            }
        });
        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.TypefacedTextView);
        String fontName = styledAttrs.getString(R.styleable.TypefacedTextView_typeface);
        styledAttrs.recycle();

        if (fontName != null) {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontName);
            setTypeface(typeface);
        }

    }

    @Override
    public void setCompoundDrawables(Drawable left, Drawable top,
                                     Drawable right, Drawable bottom) {
        if (right != null) {
            dRight = right;
        }
        super.setCompoundDrawables(left, top, right, bottom);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_UP && dRight != null) {
            rBounds = dRight.getBounds();
            final int x = (int) event.getX();
            final int y = (int) event.getY();
            //System.out.println("x:/y: "+x+"/"+y);
            //System.out.println("bounds: "+bounds.left+"/"+bounds.right+"/"+bounds.top+"/"+bounds.bottom);
            //check to make sure the touch event was within the bounds of the drawable
            if (x >= (this.getRight() - rBounds.width() - this.getPaddingLeft() * dpToPx(1) - dpToPx(4)) && x <= (this.getRight())
                    && y >= this.getPaddingTop() && y <= (this.getHeight() - this.getPaddingBottom())) {
                System.out.println("deneme");
                if (this.getText().toString().length()>0){

                    UtilsLibrary.hideSoftKeyboard(TypeEdittextWithSearchIcon.this, context);

                SearchClickInterface searchClickInterface = (SearchClickInterface) context;
                searchClickInterface.search(this.getText().toString());
                event.setAction(MotionEvent.ACTION_CANCEL);//use this to prevent the keyboard from coming up
                    this.setText("");
                }
            }
        }
        return super.onTouchEvent(event);
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    @Override
    protected void finalize() throws Throwable {
        dRight = null;
        rBounds = null;
        super.finalize();
    }

//    private boolean onClickSearch(final View view, MotionEvent event) {
//        // do something
//        EditText editText = (EditText) view;
//        editText.setText("ewfwef");
//        System.out.println("deneme deneme deneme 2");
////        event.setAction(MotionEvent.ACTION_CANCEL);
//        return true;
//    }
}
