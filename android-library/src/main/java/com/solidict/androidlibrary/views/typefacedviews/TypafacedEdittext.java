package com.solidict.androidlibrary.views.typefacedviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import com.solidict.androidlibrary.R;
import com.solidict.androidlibrary.views.valiadtorview.ValidateableEdittext;


/**
 * Created by umutkina on 03/12/14.
 */
public class TypafacedEdittext extends ValidateableEdittext {


    public TypafacedEdittext(Context context, AttributeSet attrs) {
        super(context, attrs);



        //Typeface.createFromAsset doesn't work in the layout editor. Skipping...
        if (isInEditMode()) {
            return;
        }
        setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                  setSelection(getText().toString().length());
                }
            }
        });
//        setInputType(InputType.TYPE_CLASS_TEXT);
        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.TypefacedTextView);
        String fontName = styledAttrs.getString(R.styleable.TypefacedTextView_typeface);
        styledAttrs.recycle();

        if (fontName != null) {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontName);
            setTypeface(typeface);
        }
    }


}
