package com.solidict.androidlibrary.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.PictureDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.ImageView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by umutkina on 17/12/14.
 */
public class UtilsImage {

//    public static TypedFile getTypedFileFromBitmap(Bitmap bitmap,Context context){
//        TypedFile typedFile=null;
//        File f;
//        if (bitmap != null) {
//            try {
//                f = new File(context.getCacheDir(), "mahmut.png");
//                f.createNewFile();
//
//                ByteArrayOutputStream bos = new ByteArrayOutputStream();
//                bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
//                byte[] bitmapdata = bos.toByteArray();
//                FileOutputStream fos = new FileOutputStream(f);
//                fos.write(bitmapdata);
//                fos.flush();
//                fos.close();
//                typedFile = new TypedFile("image/png", f);
//                // paramsMap.put("image", f);
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//
//        }
//
//        return  typedFile;
//    }
    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {


        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }

    public static Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
    public static  String getPath(Uri contentUri,Activity context) {
        String res = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(contentUri, proj, null, null, null);
        if(cursor.moveToFirst()){;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }
//    public static String getPath(Uri uri, Activity context) {
//        String[] projection = {MediaStore.MediaColumns.DATA};
//        Cursor cursor = context.managedQuery(uri, projection, null, null, null);
//        int column_index = cursor
//                .getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//        cursor.moveToFirst();
//        String imagePath = cursor.getString(column_index);
//
//        return cursor.getString(column_index);
//    }

    public static Bitmap getBitmapFromIntent(Intent intent, Activity activity) {
        Bitmap bitmap;
        Uri selectedImageUri = intent.getData();

        //OI FILE Manager
//            filemanagerstring = selectedImageUri.getPath();

        //MEDIA GALLERY
        String selectedImagePath = UtilsImage.getPath(selectedImageUri, activity);
        bitmap = BitmapFactory.decodeFile(selectedImagePath);
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(selectedImagePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return UtilsImage.RotateBitmap(bitmap, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return UtilsImage.RotateBitmap(bitmap, 180);


        }
        return bitmap;

    }

    public  static void  showSvgFromUrl(Activity activity, String url, ImageView imageView){
        GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder = Glide.with(activity)
                .using(Glide.buildStreamModelLoader(Uri.class, activity), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                .decoder(new SvgDecoder())
//                .placeholder(R.drawable.ic_facebook)
//                .error(R.drawable.ic_web)
                .animate(android.R.anim.fade_in)
                .listener(new SvgSoftwareLayerSetter<Uri>());

        requestBuilder
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                // SVG cannot be serialized so it's not worth to cache it
                .load( Uri.parse(url))
                .into(imageView);
    }
}
