package com.solidict.androidlibrary.edittextvalidator;

import android.widget.EditText;

/**
 * Created by umutkina on 08/02/16.
 */
public interface Validator {
    boolean validate(EditText editText);

    String getMessage(EditText editText);
}
