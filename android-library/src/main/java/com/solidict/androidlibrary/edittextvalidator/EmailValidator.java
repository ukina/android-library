package com.solidict.androidlibrary.edittextvalidator;

import android.widget.EditText;

/**
 * Created by umutkina on 08/02/16.
 */
public class EmailValidator implements Validator {
    @Override
    public boolean validate(EditText editText) {
        return isAppropriateEmail(editText.getText().toString());
    }

    @Override
    public String getMessage(EditText editText) {
        return "Lütfen uygun bir email adresi giriniz";
    }
    public  boolean isAppropriateEmail(String email) {
        return email.lastIndexOf(".") > email.indexOf("@")
                && email.indexOf("@") > 0;
    }
}
