package com.solidict.androidlibrary.edittextvalidator;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by umutkina on 08/02/16.
 */
public class ValidatorUtils {
    public static void showWarningDialog(Context context,
                                         String message) {

        final AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setTitle("Uyarı");
        alert.setMessage(message);


        alert.setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        try {
            ((Activity) context).runOnUiThread(new Runnable() {
                public void run() {
                    AlertDialog show = alert.show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean validate(Activity activity) {
        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) activity
                .findViewById(android.R.id.content)).getChildAt(0);


        return validatorView(viewGroup);
    };

    public static boolean validatorView(ViewGroup viewGroup){

        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if (childAt instanceof ValidateableView) {
                ValidateableView typafacedEdittext = (ValidateableView) childAt;
                if (!typafacedEdittext.validate()) {

                    return false;

                }

            }
            else if (childAt instanceof ViewGroup){
                if (!validatorView((ViewGroup) childAt)) {
                    return false;
                }
            }

        }

        return true;
    }


}
