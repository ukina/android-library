package com.solidict.androidlibrary.edittextvalidator;

/**
 * Created by umutkina on 08/02/16.
 */
public interface ValidateableView {

    boolean validate();
}
