package com.solidict.androidlibrary.interfaces;

/**
 * Created by umutkina on 08/12/14.
 */
public interface SearchClickInterface {
    public void search(String query);
}
