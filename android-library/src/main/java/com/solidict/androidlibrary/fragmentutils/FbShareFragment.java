package com.solidict.androidlibrary.fragmentutils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.widget.ShareDialog;


/**
 * Created by umutkina on 18/12/14.
 */
public class FbShareFragment extends Fragment {
    Bundle bundle;
    FacebookShareInterface facebookShareInterface;
    private boolean mDismissed = true;

    CallbackManager callbackManager;
    ShareDialog shareDialog;

    public void show(FragmentManager manager, String tag) {
        if (!mDismissed) {
            return;
        }
        mDismissed = false;
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void dismiss() {
        if (mDismissed) {
            return;
        }
        mDismissed = true;
        getFragmentManager().popBackStack();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.remove(this);
        ft.commit();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            facebookShareInterface = (FacebookShareInterface) getActivity();
        } catch (Exception e) {

        }


        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        // this part is optional
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                if (facebookShareInterface != null) {
                    facebookShareInterface.shareComplete();
                }

            }

            @Override
            public void onCancel() {
                if (facebookShareInterface != null) {
                    facebookShareInterface.shareFail();
                }
            }

            @Override
            public void onError(FacebookException e) {
                if (facebookShareInterface != null) {
                    facebookShareInterface.shareFail();
                }
            }
        });

    }
//
//    public static FbShareFragment newInstance(String name, String caption, String description) {
//        FbShareFragment myFragment = new FbShareFragment();
//
//        Bundle args = new Bundle();
//
//        myFragment.setArguments(args);
//
//        return myFragment;
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
// Bundle bundle = new Bundle();
//        bundle.putSerializable("name", name);
//        bundle.putSerializable("caption", caption);
//        bundle.putSerializable("description", description);
//        bundle.putString("link", link);
//        if (picture != null) {
//            bundle.putString("picture", picture);
//        }
        bundle = getArguments();
        String name = bundle.getString("name");
        String description = bundle.getString("description");
        String link = bundle.getString("link");
        String picture = bundle.getString("picture");
        if (ShareDialog.canShow(ShareOpenGraphContent.class)) {


            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(name)
                    .setContentDescription(description)
                    .setImageUrl(picture != null ? Uri.parse(picture) : Uri.parse(""))
                    .setContentUrl(Uri.parse(link))
                    .build();


            shareDialog.show(linkContent);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    public void setFacebookShareInterface(FacebookShareInterface facebookShareInterface) {
        this.facebookShareInterface = facebookShareInterface;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public static Builder createBuilder(Context context,
                                        FragmentManager fragmentManager) {
        return new Builder(context, fragmentManager);
    }


    public static class Builder {

        private Context mContext;
        private FragmentManager mFragmentManager;

        private String mTag = "fbShare";
        FacebookShareInterface fbShareInterface;
        String name,  description, link, picture;

        public Builder(Context context, FragmentManager fragmentManager) {
            mContext = context;
            mFragmentManager = fragmentManager;
        }

        public Builder setContent(String name,  String description,String link,String picture) {
            this.name = name;
            this.description = description;
            this.link = link;
            this.picture = picture;
            return this;
        }

        public Builder setListener(FacebookShareInterface fbShareInterface) {
            this.fbShareInterface = fbShareInterface;
            return this;
        }

        public Bundle prepareArguments() {
            Bundle bundle = new Bundle();
            bundle.putSerializable("name", name);
            bundle.putSerializable("description", description);
            bundle.putString("link", link);
            if (picture != null) {
                bundle.putString("picture", picture);
            }

            return bundle;
        }


        public FbShareFragment show() {
            FbShareFragment facebookFragment = (FbShareFragment) Fragment.instantiate(
                    mContext, FbShareFragment.class.getName(), prepareArguments());
            facebookFragment.setFacebookShareInterface(fbShareInterface);
            facebookFragment.show(mFragmentManager, mTag);
            return facebookFragment;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public static interface FacebookShareInterface {

        public void shareComplete();

        public void shareFail();
    }

}
