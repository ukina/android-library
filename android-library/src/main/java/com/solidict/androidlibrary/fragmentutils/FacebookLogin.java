package com.solidict.androidlibrary.fragmentutils;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by umutkina on 26/08/15.
 */
public class FacebookLogin extends Fragment {

    private static final String TAG = "facebookFragment";
    CallbackManager callbackManager;
    FacebookInterface facebookInterface;
    private boolean mDismissed = true;
//    AccessTokenTracker accessTokenTracker;
    AccessToken accessToken;
    String email;
    String name;
    String surname;
    ProfileTracker profileTracker;
    int counter;

    public void show(FragmentManager manager, String tag) {
        if (!mDismissed) {
            return;
        }
        mDismissed = false;
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void dismiss() {
        if (mDismissed) {
            return;
        }


        mDismissed = true;
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            fragmentManager.popBackStack();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.remove(this);
            ft.commit();
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        callbackManager = CallbackManager.Factory.create();


        // If the access token is available already assign it.
//        accessToken = AccessToken.getCurrentAccessToken();
//        String token = accessToken.getToken();
//        String userId = accessToken.getUserId();


//        uiHelper = new UiLifecycleHelper(getActivity(), callback);
//        uiHelper.onCreate(savedInstanceState);
//        facebookInterface = (FacebookInterface) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        onClickLogin();
//        LoginManager.getInstance().registerCallback(callbackManager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//                        // App code
//
//                    }
//
//                    @Override
//                    public void onCancel() {
//                        // App code
//                    }
//
//                    @Override
//                    public void onError(FacebookException exception) {
//                        // App code
//                    }
//                });

//        accessTokenTracker = new AccessTokenTracker() {
//            @Override
//            protected void onCurrentAccessTokenChanged(
//                    AccessToken oldAccessToken,
//                    final AccessToken currentAccessToken) {
//
//
//
//
//
////                profileTracker = new ProfileTracker() {
////                    @Override
////                    protected void onCurrentProfileChanged(
////                            Profile oldProfile,
////                            Profile currentProfile) {
////                        currentProfile.get
////                        // App code
////                    }
////                };
//                // Set the access token using
//                // currentAccessToken when it's loaded or set.
//            }
//
//        };

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        final AccessToken accessToken = loginResult.getAccessToken();
                        GraphRequest request = GraphRequest.newMeRequest(
                                accessToken,
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        try {
                                            name = object.getString("first_name");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            surname = object.getString("last_name");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        try {


                                            if (!object.isNull("email")) {
                                                email = object.getString("email");
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        facebookInterface.fbLogin(accessToken.getUserId(), accessToken.getToken(), email, name, surname);
                                    }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "email,name,first_name,last_name");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                            }
                        }
                        exception.printStackTrace();
                        // App code
                    }
                });
        LoginManager.getInstance().logInWithReadPermissions(FacebookLogin.this, Arrays.asList("public_profile", "email", "user_friends"));

        return super.onCreateView(inflater, container, savedInstanceState);
    }


    public void setFacebookInterface(FacebookInterface facebookInterface) {
        this.facebookInterface = facebookInterface;
    }

    public static Builder createBuilder(Context context,
                                        FragmentManager fragmentManager) {
        return new Builder(context, fragmentManager);
    }


    public static class Builder {

        private Context mContext;
        private FragmentManager mFragmentManager;

        private String mTag = "facebookFragment";
        FacebookInterface facebookInterface;

        public Builder(Context context, FragmentManager fragmentManager) {
            mContext = context;
            mFragmentManager = fragmentManager;
        }


        public Builder setListener(FacebookInterface facebookInterface) {
            this.facebookInterface = facebookInterface;
            return this;
        }
//        public Bundle prepareArguments() {
//            Bundle bundle = new Bundle();
//            bundle.putInt(videoLimitKey, videoLimit);
//
//            return bundle;
//        }


        public FacebookLogin show() {
            FacebookLogin facebookFragment = (FacebookLogin) Fragment.instantiate(
                    mContext, FacebookLogin.class.getName());
            facebookFragment.setFacebookInterface(facebookInterface);
            facebookFragment.show(mFragmentManager, mTag);
            return facebookFragment;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
//        uiHelper.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
//        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
//        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();


//        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        uiHelper.onSaveInstanceState(outState);
    }

    public static interface FacebookInterface {

        public void fbLogin(String fbId, String accesToken, String email, String name, String surname);
    }

}

