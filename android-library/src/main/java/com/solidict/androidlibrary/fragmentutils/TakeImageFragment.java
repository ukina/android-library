package com.solidict.androidlibrary.fragmentutils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.solidict.androidlibrary.utils.UtilsImage;


/**
 * Created by umutkina on 07/04/15.
 */
public class TakeImageFragment extends Fragment {
    private static String takeImageLimitKey = "takeImageLimit";
    public static final int RESULT_OK = -1;
    public static final int RESULT_CANCELED = 0;
    private static final int CAPTURE_takeImage_ACTIVITY_REQUEST_CODE = 200;

    private Uri fileUri;
    private boolean mDismissed = true;
    static int TAKE_PICTURE = 1;
    Bitmap bitMap;

    TakeImageInterface takeImageInterface;
    public void show(FragmentManager manager, String tag) {
        if (!mDismissed) {
            return;
        }
        mDismissed = false;
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void dismiss() {
        if (mDismissed) {
            return;
        }
        mDismissed = true;
        getFragmentManager().popBackStack();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.remove(this);
        ft.commit();
    }

    public TakeImageInterface getTakeImageInterface() {
        return takeImageInterface;
    }

    public void setTakeImageInterface(TakeImageInterface takeImageInterface) {
        this.takeImageInterface = takeImageInterface;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive()) {
            View focusView = getActivity().getCurrentFocus();
            if (focusView != null) {
                imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
            }
        }


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        startActivityForResult(intent, TAKE_PICTURE);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        dismiss();

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK && data != null) {
            // get bundle
            Bundle extras = data.getExtras();

            // get
            bitMap = (Bitmap) extras.get("data");

        }
        if (resultCode == RESULT_OK) {
            bitMap = UtilsImage.getResizedBitmap(bitMap, (int) (320 * ((double) bitMap.getHeight() / bitMap.getWidth())), 320);

            takeImageInterface = (TakeImageInterface) getActivity();
            takeImageInterface.useImage(bitMap);
        }
    }

    public static interface TakeImageInterface {

        public void useImage(Bitmap bitmap);
    }


  
    public static Builder createBuilder(Context context,
                                        FragmentManager fragmentManager) {
        return new Builder(context, fragmentManager);
    }


    public static class Builder {

        private Context mContext;
        private FragmentManager mFragmentManager;

        private String mTag = "takeImageCapture";
        TakeImageInterface takeImageInterface;
        private int takeImageLimit;

        public Builder(Context context, FragmentManager fragmentManager) {
            mContext = context;
            mFragmentManager = fragmentManager;
        }


        public Builder setTag(String tag) {
            mTag = tag;
            return this;
        }



        public Builder setListener(TakeImageInterface takeImageInterface) {
            this.takeImageInterface = takeImageInterface;
            return this;
        }

        private Bundle prepareArguments() {
            Bundle bundle = new Bundle();
            bundle.putInt(takeImageLimitKey, takeImageLimit);

            return bundle;
        }


        public TakeImageFragment show() {
            TakeImageFragment takeImageCapture = (TakeImageFragment) Fragment.instantiate(
                    mContext, TakeImageFragment.class.getName(), prepareArguments());
            takeImageCapture.setTakeImageInterface(takeImageInterface);
            takeImageCapture.show(mFragmentManager, mTag);
            return takeImageCapture;
        }

    }
}
