package com.solidict.androidlibrary.fragmentutils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by umutkina on 26/01/15.
 */
public class DatePickerFragment extends DialogFragment

        implements DatePickerDialog.OnDateSetListener {
    int age;
    //    TextView tvDate;
    OnDateSelectedListener onDateSelectedListener;

    //    public DatePickerFragment(TextView tvDate) {
//        this.tvDate = tvDate;
//    }
    public static String fragmentTagKey = "FRAGMENT_TAG";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
// Use the current date as the default date in the picker
        age = getArguments().getInt("age");
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR)-age;
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
// Create a new instance of DatePickerDialog and return it
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis()- 1000);

        return datePickerDialog;
    }

    public static final DatePickerFragment newInstance(int fragmetnTag) {
        DatePickerFragment fragment = new DatePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(fragmentTagKey, fragmetnTag);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static final DatePickerFragment newInstance(int fragmetnTag,int year) {
        DatePickerFragment fragment = new DatePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(fragmentTagKey, fragmetnTag);
        bundle.putInt("age", year);
        fragment.setArguments(bundle);
        return fragment;
    }
    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);

        int fragmentTag = getArguments().getInt(fragmentTagKey);


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = sdf.format(c.getTime());
        if (onDateSelectedListener != null) {
            onDateSelectedListener.OnDateSelected(formattedDate, fragmentTag);
        }


    }

    public OnDateSelectedListener getOnDateSelectedListener() {
        return onDateSelectedListener;
    }

    public void setOnDateSelectedListener(OnDateSelectedListener onDateSelectedListener) {
        this.onDateSelectedListener = onDateSelectedListener;
    }

    public static interface OnDateSelectedListener {
        public abstract void OnDateSelected(String formattedDate, int fragmentTag
        );
    }
}