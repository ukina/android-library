package com.solidict.androidlibrary.fragmentutils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.VideoView;

import java.io.File;

/**
 * Created by umutkina on 06/04/15.
 */
public class VideoCaptureFragment extends Fragment {
    private static String videoLimitKey = "videoLimit";
    public static final int RESULT_OK = -1;
    public static final int RESULT_CANCELED = 0;
    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;

    private Uri fileUri;
    VideoView vw;
    VideoCaptureInterface videoCaptureInterface;
    private boolean mDismissed = true;

    public void show(FragmentManager manager, String tag) {
        if (!mDismissed) {
            return;
        }
        mDismissed = false;
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void dismiss() {
        if (mDismissed) {
            return;
        }
        mDismissed = true;
        getFragmentManager().popBackStack();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.remove(this);
        ft.commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive()) {
            View focusView = getActivity().getCurrentFocus();
            if (focusView != null) {
                imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
            }
        }


        int videoLimit = getArguments().getInt(videoLimitKey);

        Intent cameraIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, videoLimit);

        fileUri = getOutputMediaFileUri();  // create a file to save the video
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);  // set the image file name

        cameraIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0); // set the video image quality to high

        // start the Video Capture Intent
        startActivityForResult(cameraIntent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        dismiss();

        if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Video captured and saved to fileUri specified in the Intent

                videoCaptureInterface.useFile(new File(fileUri.getPath()));

            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the video capture
            } else {
                // Video capture failed, advise user
            }
        }
    }


    public void setVideoCaptureInterface(VideoCaptureInterface videoCaptureInterface) {
        this.videoCaptureInterface = videoCaptureInterface;
    }

    private Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

//        File mediaStorageDir = new File(getCacheDir(), "MyCameraApp");
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "VID_" + "pso_video" + ".mp4");


        return mediaFile;
    }

    public static interface VideoCaptureInterface {

        public void useFile(File file);
    }

    public static Builder createBuilder(Context context,
                                        FragmentManager fragmentManager) {
        return new Builder(context, fragmentManager);
    }


    public static class Builder {

        private Context mContext;
        private FragmentManager mFragmentManager;

        private String mTag = "videoCapture";
        VideoCaptureInterface videoCaptureInterface;
        private int videoLimit;

        public Builder(Context context, FragmentManager fragmentManager) {
            mContext = context;
            mFragmentManager = fragmentManager;
        }


        public Builder setTag(String tag) {
            mTag = tag;
            return this;
        }

        public Builder setVideoLimit(int videoLimit) {
            this.videoLimit = videoLimit;
            return this;
        }

        public Builder setListener(VideoCaptureInterface videoCaptureInterface) {
            this.videoCaptureInterface = videoCaptureInterface;
            return this;
        }

        private Bundle prepareArguments() {
            Bundle bundle = new Bundle();
            bundle.putInt(videoLimitKey, videoLimit);

            return bundle;
        }


        public VideoCaptureFragment show() {
            VideoCaptureFragment videoCapture = (VideoCaptureFragment) Fragment.instantiate(
                    mContext, VideoCaptureFragment.class.getName(), prepareArguments());
            videoCapture.setVideoCaptureInterface(videoCaptureInterface);
            videoCapture.show(mFragmentManager, mTag);
            return videoCapture;
        }

    }
}
